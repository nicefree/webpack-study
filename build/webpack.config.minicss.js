// webpack.config.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin")
module.exports = {
    mode: 'production', // 开发模式
    entry: path.resolve(__dirname,'../src/main.js'),    // 入口文件
    output: {
        filename: '[name].[fullhash:8].js',      // 打包后的文件名称
        path: path.resolve(__dirname,'../dist')  // 打包后的目录
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname,'../public/index.html'),
            inject: 'head',
            scriptLoading: 'blocking'
        }),
        new MiniCssExtractPlugin({
            filename: "[name].[fullhash].css",
            chunkFilename: "[id].css",
        })
    ], 
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [ { loader: MiniCssExtractPlugin.loader }, "css-loader", "postcss-loader"], // 从右向左解析原则
            },
            {
                test: /\.less$/,
                use: [ { loader: MiniCssExtractPlugin.loader },  "css-loader", "postcss-loader", "less-loader"], // 从右向左解析原则
            },
            {
                test: /\.scss$/,
                use: [ { loader: MiniCssExtractPlugin.loader },  "css-loader", "postcss-loader", "sass-loader"], // 从右向左解析原则
            },
            {
                test: /\.(jpe?g|png|gif)$/i, // 图片文件
                type: "asset",
                // 解析
                parser: {
                  // 超过10kb将转成base64，优点：减少请求 缺点：文件体积变大
                  dataUrlCondition: {
                    maxSize: 10 * 1024,
                  }
                },
                generator:{ 
                  // 与output.assetModuleFilename是相同的,这个写法引入的时候也会添加好这个路径
                  filename: 'images/[name].[hash:6][ext]',
                  // 打包后对资源的引入
                  // publicPath: '../'
                },
            },
        ]
    },
    optimization: {
        minimizer: [new CssMinimizerPlugin()]
    }
    
}
