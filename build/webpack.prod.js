const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin')
const HappyPack = require('happypack')

module.exports = merge(common, {
  mode: "production",
  module: {
    rules: [
      {
        test: /\.js$/,
        // use: ['babel-loader?cacheDirectory'],
        // 改为使用 happypack打包
        use: ['happypack/loader?id=babel'], // 这个id是自定义命名的，要跟插件中id对应 
        // 排除 node_modules 目录下的文件
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      analyzerHost: '127.0.0.1',
      analyzerPort: 8888, 
      reportFilename: 'report.html',
      defaultSizes: 'parsed',
      openAnalyzer: false,
      generateStatsFile: false, 
      statsFilename: 'stats.json',
      statsOptions: null,
      logLevel: 'info'
    }),
    new ParallelUglifyPlugin({
      // 压缩js的一些配置
      uglifyJS: {
        output: {
          beautify: false,  // 不需要格式化，以最紧凑的方式输出
          comments: false // 删除注释
        },
        warnings: false, // 删除未使用一些代码时引起的警告
        compress: {
          drop_console: true, // 删除所有console.log
          // 是否内嵌虽定义，但只使用了一次的变量
          // 比如var x = 2, y = 10, z = x + y 变成 z = 12
          collapse_vars: true,
          // 提出多次出现但没定义的变量，将其变成静态值；
          // 比如x = 'xx', y = 'xx' 变成 var a = 'xx', x = a, y = a 
          reduce_vars: true 
        }
      }
    }),
    new HappyPack({
      id: 'babel', // 唯一标识符
      // 使用的loader配置改写到happypack的配置项中
      use: ['babel-loader']
    })
  ],
  optimization: {
    splitChunks: {
      chunks: "all",
      name: "vendor",
      cacheGroups: {
        "echarts.vendor": {
          name: "echarts.vendor",
          priority: 40,
          test: /[\\/]node_modules[\\/](echarts|zrender)[\\/]/,
          chunks: "all",
        },
        lodash: {
          name: "lodash",
          chunks: "async",
          test: /[\\/]node_modules[\\/]lodash[\\/]/,
          priority: 40,
        },
        "async-common": {
          chunks: "async",
          minChunks: 2,
          name: "async-commons",
          priority: 30,
        },
        commons: {
          name: "commons",
          chunks: "all",
          minChunks: 2,
          priority: 20,
        },
      },
    },
  },
  externals: {
    jquery: 'jQuery',
  },
})