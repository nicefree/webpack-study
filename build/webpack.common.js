// webpack.common.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const chalk = require("chalk")
const ProgressBarPlugin = require("progress-bar-webpack-plugin")
const WebpackBar = require('webpackbar')
console.log('process.env.NODE_ENV', process.env.NODE_ENV)
module.exports = {
    entry: path.resolve(__dirname,'../src/main.js'),    // 入口文件
    output: {
        filename: '[name].[fullhash:8].js',      // 打包后的文件名称
        path: path.resolve(__dirname,'../dist')  // 打包后的目录
    },
    resolve: {
        alias: { // 配置别名
          "@": path.resolve(__dirname, "../src"),
        },
    },
    plugins: [
        new WebpackBar(),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname,'../public/index.html'),
            inject: 'body',
            scriptLoading: 'blocking'
        }),
        new CopyPlugin({
            patterns: [
              {
                from: path.resolve(__dirname, '../public'),
                to: './',
                globOptions: {
                  ignore: ['**/*.html'],
                },
              },
            ],
        }),
    ], 
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader", "postcss-loader"], // 从右向左解析原则
            },
            {
                test: /\.less$/,
                use: ["style-loader", "css-loader", "postcss-loader", "less-loader"], // 从右向左解析原则
            },
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "postcss-loader", "sass-loader"], // 从右向左解析原则
            },
            {
                test: /\.(jpe?g|png|gif)$/i, // 图片文件
                type: "asset",
                // 解析
                parser: {
                  // 超过10kb将转成base64，优点：减少请求 缺点：文件体积变大
                  dataUrlCondition: {
                    maxSize: 10 * 1024,
                  }
                },
                generator:{ 
                  // 与output.assetModuleFilename是相同的,这个写法引入的时候也会添加好这个路径
                  filename: 'images/[name].[hash:6][ext]',
                  // 打包后对资源的引入
                  publicPath: './'
                },
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/, // 媒体文件
                type: "asset/resource",
                generator: {
                  filename: "media/[name].[hash:6][ext]",
                  publicPath: "./",
                },
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i, // 字体
                type: "asset/inline", // inline 的时候不需要指定文件名
            },
            {
                test: /\.js$/,
                use: {
                  loader: "babel-loader",
                  /* options: {
                    presets: ["@babel/preset-env"]
                  } */
                },
                exclude: /node_modules/
            },
 
        ]
    },
    
}
