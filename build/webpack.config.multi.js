// webpack.config-multi.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    mode: 'development', // 开发模式
    entry: {
        main: path.resolve(__dirname,'../src/main.js'),
        header: path.resolve(__dirname,'../src/header.js')
    },
    output: {
        filename: '[name].[fullhash:8].js',      // 打包后的文件名称
        path: path.resolve(__dirname,'../dist')  // 打包后的目录
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname,'../public/index.html'),
            filename: 'index.html',
            inject: 'head',
            scriptLoading: 'blocking',
            chunks: ['main'] // 与入口文件对应的模块名
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname,'../public/header.html'),
            filename: 'header.html',
            inject: 'head',
            scriptLoading: 'blocking',
            chunks: ['header'] // 与入口文件对应的模块名
        }),
    ]
}
